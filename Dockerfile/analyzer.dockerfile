FROM golang:1.13 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

# Build project-specific dependencies

ENTRYPOINT []
CMD ["/analyzer", "run"]


